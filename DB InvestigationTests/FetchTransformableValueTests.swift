//
//  DB_InvestigationTests.swift
//  DB InvestigationTests
//
//  Created by admin on 6/19/17.
//  Copyright © 2017 admin. All rights reserved.
//

import XCTest
@testable import DB_Investigation

class FetchTransformableValueTests: XCTestCase {
   
    var items : [Staff]? = nil
    var randomObject : Staff? = nil
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        ValueTransformer.setValueTransformer(UUIDValueTransformer(), forName: NSValueTransformerName("UUIDValueTransformer"))
        DBController.shared.setupDB()
        
        do {
            self.items = try DBController.shared.fetchAllItems(entityName: String(describing: Staff.self))
            if self.items?.count == 0 {
                self.items = DBController.shared.generateRandomItems(count: kTestedItemsCountValue)
            }
        } catch {
        }
        if let items = items {
            let rand = arc4random_uniform(UInt32(items.count))
            self.randomObject = items[Int(rand)]
        }
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            if let randomObject = self.randomObject, let idTransformableValue = randomObject.idTransformableValue {
                _ = DBController.shared.fetchItemByTransformableId(idObject: idTransformableValue as UUID)
            }
        }
    }
    
}
