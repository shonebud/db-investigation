//
//  QQQObject.swift
//  DB Investigation
//
//  Created by admin on 6/19/17.
//  Copyright © 2017 admin. All rights reserved.
//

import Foundation

public class QQQObject: NSObject, NSCoding {
    var name : String
    var staff : String
    
    init(name : String, staff : String) {
        self.name = name
        self.staff = staff
    }
    
    public required convenience init(coder aDecoder: NSCoder) {
        let name = aDecoder.decodeObject(forKey: "name") as! String
        let staff = aDecoder.decodeObject(forKey: "staff") as! String
        self.init(
            name: name,
            staff: staff
        )
    }
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(name, forKey: "name")
        aCoder.encode(staff, forKey: "staff")
    }
}
