//
//  DBController.swift
//  DB Investigation
//
//  Created by admin on 6/16/17.
//  Copyright © 2017 admin. All rights reserved.
//

import Foundation
import CoreData

public class DBController: NSObject {
    
    static var shared = DBController()
    var mainContext : NSManagedObjectContext!
    
    override init() {
        super.init()
    }
    
    func setupDB() {
        let modelURL = Bundle.main.url(forResource: "DB_Investigation", withExtension: "momd")
        assert(modelURL != nil,"Failed to find model")
        
        if let modelURL = modelURL, let mom = NSManagedObjectModel(contentsOf: modelURL) {
            let psc = NSPersistentStoreCoordinator(managedObjectModel: mom)
            self.mainContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
            self.mainContext?.persistentStoreCoordinator = psc;
            
            let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
            let docURL = urls[urls.endIndex-1]
            let storeURL = docURL.appendingPathComponent("DB_Investigation_Model.sqlite")
            print("--- Store url: ---\n\(storeURL)")
            do {
                try psc.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: storeURL, options: nil)
            } catch {
                fatalError("Error migrating store: \(error)")
            }
        }
    }
    
    func saveContext () {
        if let mainContext = mainContext, mainContext.hasChanges {
            do {
                try mainContext.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
}

extension DBController {
    
//    func randomStringWithLength(len: Int) -> String {
//        let letters = [Character]("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".characters)
//        var characters = [Character]()
//        let length = UInt32 (letters.count)
//        
//        for _ in 0...len-1 {
//            let rand = arc4random_uniform(length)
//            characters.append(letters[Int(rand)])
//        }
//        
//        return String(characters)
//    }
    
    func generateRandomItems(count : Int) -> [Staff] {
        var items = [Staff]()
        for _ in 0...count-1 {
            let entity = NSEntityDescription.entity(forEntityName: String(describing: "Staff"),
                                                    in: mainContext)!
            let staffItem : Staff! = NSManagedObject(entity: entity, insertInto: mainContext) as! Staff
            
            staffItem.createDate = NSDate()
            staffItem.idStringValue = UUID().uuidString //randomStringWithLength(len: 10)
            
//            let object = QQQObject(name:  UUID().uuidString, staff:  UUID().uuidString)
            staffItem.idTransformableValue = UUID()
           
            items.append(staffItem)
        }
        
        self.saveContext()
        return items
    }
    
    func fetchItemByStrId(id : String) -> Staff? {
        let request : NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: String(describing: Staff.self))
        let predicate = NSPredicate(format: "%K == %@", #keyPath(Staff.idStringValue), id)
        request.predicate = predicate
        do {
            let fetchedEntities = try mainContext.fetch(request) as! [Staff]
            return fetchedEntities.first
        } catch {
            print(error)
        }
        return nil
    }
    
    func fetchItemByTransformableId(idObject : UUID) -> Staff? {
        let request : NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: String(describing: Staff.self))
        let predicate = NSPredicate(format: "%K == %@", argumentArray: [#keyPath(Staff.idTransformableValue), idObject])
        request.predicate = predicate
        do {
            let fetchedEntities = try mainContext.fetch(request) as! [Staff]
            return fetchedEntities.first
        } catch {
            print(error)
        }
        return nil
    }
    
    func fetchAllItems(entityName : String) throws -> [Staff]? {
        let request : NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: String(describing: Staff.self))
        do {
            let fetchedEntities = try mainContext.fetch(request) as! [Staff]
            return fetchedEntities
        } catch {
            print(error)
        }
        return nil
    }
    
//    func fetchItemByStrId(id : String) -> Staff? {
//        let request : NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: String(describing: Staff.self))
//        request.predicate = NSPredicate(format: "idStringValue == \(id)")
//        do {
//            let fetchedEntities = try mainContext.fetch(request) as! [Staff]
//            return fetchedEntities.first
//        } catch {
//            print(error)
//        }
//        return nil
//    }
    
    func deleteAllItems(entityName : String) throws -> NSPersistentStoreResult {
        let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        let request = NSBatchDeleteRequest(fetchRequest: fetch)
        return try mainContext.execute(request)
    }
}
