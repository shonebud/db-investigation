//
//  ViewController.swift
//  DB Investigation
//
//  Created by admin on 6/16/17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var startDate : Date?
    var items : [Staff]? = nil
    var randomObject : Staff? = nil
    
    var sum : Double = 0.0
    var sumCount : Int = 0
    
    @IBOutlet weak var logTextView: UITextView!
    @IBOutlet weak var countTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func generateRandomItems(_ sender: Any) {
        startPerfomance()
        
        if let contStr = countTextField.text, let count = Int(contStr) {
             self.items = DBController.shared.generateRandomItems(count: count)
        }
        _ = finishPerfomance()
        
        print("Finished")
        
    }
    
    @IBAction func getRandomObject(_ sender: Any) {
        if items == nil {
            fetchAllItems(sender)
        }
        if let items = items {
            let rand = arc4random_uniform(UInt32(items.count))
            randomObject = items[Int(rand)]
            if let randomObject = randomObject {
                log("---Random Object---\n\(randomObject)")
            }
        }
    }
    
    @IBAction func fetchByStr(_ sender: Any) {
        if randomObject == nil {
            getRandomObject(sender)
        }
        
        if let randomObject = randomObject, let idStringValue = randomObject.idStringValue {
            startPerfomance()
            _ = DBController.shared.fetchItemByStrId(id: idStringValue)
            sum += finishPerfomance()
            sumCount += 1
        }
    }
    
    @IBAction func fetchByTransformable(_ sender: Any) {
        if randomObject == nil {
            getRandomObject(sender)
        }
        
        if let randomObject = randomObject, let idTransformableValue = randomObject.idTransformableValue {
            startPerfomance()
            _ = DBController.shared.fetchItemByTransformableId(idObject: idTransformableValue as UUID)
            sum += finishPerfomance()
            sumCount += 1
        }
    }
    @IBAction func resetAction(_ sender: Any) {
        do {
            startPerfomance()
            _ = try DBController.shared.deleteAllItems(entityName: String(describing: Staff.self))
        } catch {
            
        }
    }
    
    @IBAction func fetchAllItems(_ sender: Any) {
        do {
            startPerfomance()
            items = try DBController.shared.fetchAllItems(entityName: String(describing: Staff.self))
            _ = finishPerfomance()
            
            if let items = items {
                log("Items count:" + String(items.count))
            }
        } catch {
            
        }
    }
    
//    @IBAction func fetchAllItemsCount(_ sender: Any) {
//        do {
//            if let items = try DBController.shared.fetchAllItems(entityName: String(describing: Staff.self)) {
//                log(String(items.count))
//            }
//        } catch {
//            
//        }
//    }
    
    @IBAction func getAverage(_ sender: Any) {
        log(String(sum / Double(sumCount)))
    }
    
    @IBAction func resetAverage(_ sender: Any) {
        sum = 0.0
        sumCount = 0
    }
    
    @IBAction func cleanActoin(_ sender: Any) {
        logTextView.text = "--- Log ---"
    }
}

extension ViewController {
    func log(_ string : String) {
        logTextView.text = logTextView.text + "\n" + string
        
        let stringLength:Int = logTextView.text.characters.count
        logTextView.scrollRangeToVisible(NSMakeRange(stringLength-1, 0))
    }
    
    func startPerfomance() {
        startDate = Date()
    }
    
    func finishPerfomance() -> Double {
        if let startDate = startDate {
            let interval = Date().timeIntervalSince(startDate)
            log(String(interval) + "s")
            return interval
        }
        return 0
    }
}
