//
//  Staff.swift
//  DB Investigation
//
//  Created by admin on 6/16/17.
//  Copyright © 2017 admin. All rights reserved.
//

import CoreData

extension Staff {
    override public var description: String {
        get {
            return "idStringValue : \(idStringValue!)\nidTransformableValue : \(String(describing: idTransformableValue))"
        }
    }
}
