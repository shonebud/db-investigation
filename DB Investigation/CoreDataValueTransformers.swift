//
//  CoreDataValueTransformers.swift
//  TrueView
//
//  Created by Alexander Bespalov on 4/5/17.
//  Copyright © 2017 Deloitte. All rights reserved.
//

import Foundation

class UUIDValueTransformer: ValueTransformer {

    override class func transformedValueClass() -> AnyClass {
        return NSData.self
    }

    override class func allowsReverseTransformation() -> Bool {
        return true
    }

    override func transformedValue(_ value: Any?) -> Any? {
        switch value {
        case var uuid as UUID:
            return uuid.data()
        case let string as String:
            var uuid = UUID(uuidString: string)
            return uuid?.data()
        default:
            return nil
        }
    }

    override func reverseTransformedValue(_ value: Any?) -> Any? {
        return (value as? NSData).flatMap({ $0.getUUID() })
    }
}


private extension UUID {
    mutating func data() -> NSData {
        let data = withUnsafePointer(to: &self, { (pointer) -> NSData in
            return NSData(bytes: pointer, length: MemoryLayout.size(ofValue: uuid))
        })
        return data
    }

}

private extension NSData {
    
    func getUUID() -> UUID? {
        guard self.length == MemoryLayout<UUID>.size else {
            return nil
        }
        let pointer = UnsafeMutablePointer<UUID>.allocate(capacity: 1)
        defer { pointer.deallocate(capacity: 1) }
        let source = self.bytes.bindMemory(to: UUID.self, capacity: 1)
        pointer.initialize(from: source, count: 1)
        return pointer.move()
    }
    
}

