//
//  Staff+CoreDataProperties.swift
//  DB Investigation
//
//  Created by admin on 6/19/17.
//  Copyright © 2017 admin. All rights reserved.
//

import Foundation
import CoreData


extension Staff {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Staff> {
        return NSFetchRequest<Staff>(entityName: "Staff")
    }

    @NSManaged public var createDate: NSDate?
    @NSManaged public var idStringValue: String?
    @NSManaged public var idTransformableValue: UUID?
    @NSManaged public var name: String?
    @NSManaged public var staffData: NSData?

}
